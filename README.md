Command-line calculator
=======================

Provides console script `,` - hence the name *CommaCalc*.

Ships all in the `math` module and many more.
